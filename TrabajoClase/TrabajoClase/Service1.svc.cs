﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace TrabajoClase {
	// NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
	// NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
	public class Service1 : IService1 {

		public int Calcular(string operacion) {
			int a, b, pos;

			pos = operacion.IndexOf("-");
			if (pos > 0) {
				a = Convert.ToInt32(operacion.Substring(0, pos));
				b = Convert.ToInt32(operacion.Substring(pos+1));
				return a - b;
			}

			pos = operacion.IndexOf("+");
			if (pos > 0) {
				a = Convert.ToInt32(operacion.Substring(0, pos));
				b = Convert.ToInt32(operacion.Substring(pos + 1));
				return a + b;
			}
		
			pos = operacion.IndexOf("/");
			if (pos > 0) {
				a = Convert.ToInt32(operacion.Substring(0, pos));
				b = Convert.ToInt32(operacion.Substring(pos + 1));
				return a / b;
			}

			pos = operacion.IndexOf("*");
			if (pos > 0) {
				a = Convert.ToInt32(operacion.Substring(0, pos));
				b = Convert.ToInt32(operacion.Substring(pos + 1));
				return a * b;
			}

			return -1;
		}
	}
}
